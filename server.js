require("dotenv").config();

const express = require("express");
const app = express();
const mongoose = require("mongoose");
const port = process.env.PORT;

mongoose.connect(process.env.MONGODB_CONNECTION, {
  // Can remove this since mongoose 6
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", (error) => {
  error.message;
});
db.once("open", () => {
  console.log("Connected to DB");
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const usersRoute = require("./routes/usersRoute");
app.use("/users", usersRoute);

app.listen(port, () => {
  console.log(`Listening to Port ${port}`);
});
