const express = require("express");
const usersRoute = express.Router();
const User = require("../models/users");

// GET All
usersRoute.get("/", async (req, res) => {
  try {
    const Users = await User.find();
    res.status(200);
    res.json(Users);
  } catch (error) {
    res.status(500);
    res.json({ message: error.message });
  }
});

// GET One
usersRoute.get("/username", async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    res.status(200);
    res.json(user);
  } catch (error) {
    res.status(404);
    res.json({ message: error.message });
  }
});

// POST with duplication checker
usersRoute.post("/signup", async (req, res) => {
  const user = await User.findOne({ username: req.body.username });

  if (!user) {
    const newUserCreds = new User({
      username: req.body.username,
      password: req.body.password,
    });
    try {
      const newUser = await newUserCreds.save();
      res.status(201);
      res.json(newUser);
    } catch (error) {
      res.status(400);
      res.json({ message: error.message });
    }
    return;
  }

  return res.json("User Already Registered");
});

module.exports = usersRoute;
